<?php

namespace App\Http\Controllers\Products;

use App\Models\Products\Product;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Categories\Categories;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use DB;
use Alert;

class ProductController extends Controller
{
    // middleware para que solo deje pasar a las vistas los usuarios autenticados
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    //Display a listing of the resource.
    public function index()
    {
        $products=Product::orderBy('id', 'DESC')->get();
        $categories=Categories::orderBy('name', 'DESC')->get();
        return view('products.index', compact('products', 'categories'));
    }

    public function indexApi(){
        $products=Product::orderBy('id', 'DESC')->get();
        // con este modelo estoy capturando los datos de las relaciones desde el modelo
        foreach ($products as $key => $value) {
            $value->categoryname;
            $value->image;
        }
        return $products;
    }


    public function create()
    {
        return view('products.create');
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'category_id'=> ['required'],
            'description'=> ['required'],
            'price'=> ['required'],
            ]);

        $datos=collect();
        $datos->put('category_id', $request->category_id);
        $datos->put('description', $request->description);
        $datos->put('price', $request->price);
        $datos->put('user_id',Auth::id());
        // insertGetId se utiliza para insertar en una table y recuperar el id para ser utilizado en otro proceso
        $id=Product::insertGetId($datos->toArray());


        //recuperar ID y almacenar imagenes en image_path
        if($request->img1) {
            $path1 = $request->file('img1')->store('img_products','public');
            DB::table('image_path')->insert([
                'product_id'=>$id,
                'path'=>$path1
            ]);
        }

        if($request->img2) {
            $path2 = $request->file('img2')->store('img_products','public');
            DB::table('image_path')->insert([
                'product_id'=>$id,
                'path'=>$path2
            ]);
        }

        if($request->img3) {
            $path3 = $request->file('img3')->store('img_products','public');
            DB::table('image_path')->insert([
                'product_id'=>$id,
                'path'=>$path3
            ]);
        }

        alert()->success('Success Message', 'Registro creado con éxito')->autoclose(3500);
        return redirect()->route('products.index');
    }

    public function show($id)
    {
        $products=Product::find($id);
        //dd($products->image);
        return view('products.show',compact('products'));
    }


    public function edit($id)
    {
        //
        $products=Product::find($id);
        return view('products.edit', compact('products'));
    }

    //Update the specified resource in storage.
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $this->validate($request, [
            'category_id'=> ['required'],
            'description'=> ['required'],
            'price'=> ['required'],
            ]);
        $datos=collect();
        $datos->put('category_id', $request->category_id);
        $datos->put('description', $request->description);
        $datos->put('price', $request->price);
        $datos->put('user_id',Auth::id());
        Product::find($id)->update($datos->toArray());

       //recuperar ID y almacenar imagenes en image_path ('OJO FALTA CONTROLAR QUE BORRE LA IMAGEN EXISTENTE Y GUARDE LA NUEVA')
        if($request->img1) {
            $path1 = $request->file('img1')->store('img_products','public');
            DB::table('image_path')->insert([
                'product_id'=>$id,
                'path'=>$path1
            ]);
        }

        if($request->img2) {
            $path2 = $request->file('img2')->store('img_products','public');
            DB::table('image_path')->insert([
                'product_id'=>$id,
                'path'=>$path2
            ]);
        }

        if($request->img3) {
            $path3 = $request->file('img3')->store('img_products','public');
            DB::table('image_path')->insert([
                'product_id'=>$id,
                'path'=>$path3
            ]);
        }

        alert()->success('Success Message', 'Registro actualizado con éxito')->autoclose(3500);
        return redirect()->route('products.index');
    }


    public function destroy($id)
    {
        //
        Product::find($id)->delete();
        alert()->success('Success Message', 'Registro Eliminado Correctamente')->autoclose(3500);
        return redirect()->route('products.index');
    }
}
