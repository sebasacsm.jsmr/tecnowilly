<?php

namespace App\Http\Controllers\Users;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;


class UserController extends Controller
{


    // middleware para que solo deje pasar a las vistas los usuarios autenticados
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }


    public function index()
    {

        $users=User::orderBy('id', 'DESC')->paginate(10);
        return view('auth.register', compact('users'));
    }


    public function show($id)
    {
        $users=User::find($id);
        return view(' user.show',compact('users'));
    }



    public function edit($id)
    {
        $users=User::find($id);
        return view('user.edit', compact('users'));
    }



    public function update(Request $request, $id)
    {

    $data=$request->all();
    $hash=bcrypt($request->password);
    $data['password']=$hash;
        $this->validate($request, [
            'name' => ['required','string','max:255'],
            'email' => ['required', 'string', 'email', 'max:255',
            Rule::unique('users', 'email')->ignore($id),
        ],
            'password' => ['required','string','min:8','confirmed'],
        ]);
        User::find($id)->update($data);
        alert()->success('Success Message', 'Usuario modificado con éxito')->autoclose(3500);
        return redirect()->route('user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        alert()->success('Success Message', 'Usuario Eliminado Correctamente')->autoclose(3500);
        return redirect()->route('user.index');
    }
}
