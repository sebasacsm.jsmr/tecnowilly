<?php

namespace App\Http\Controllers\Categories;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Alert;
use App\Http\Controllers\Controller;
use App\Models\Categories\Categories;


class CategoriesController extends Controller
{

    // middleware para que solo deje pasar a las vistas los usuarios autenticados
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }


    //Display a listing of the resource.
    public function index()
    {
        $categories=Categories::orderBy('id', 'DESC')->paginate(10);
        return view('categories.index', compact('categories'));
    }

    //Show the form for creating a new resource.
    public function create()
    {
        //
        return view('Categories.create');
    }


    //Store a newly created resource in storage.
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name'=> ['required','unique:categories'],
            'description'=> ['required']]);



        $datos=collect($request->all());
        $datos->put('user_id',Auth::id());
        Categories::create($datos->toArray());
        alert()->success('Success Message', 'Registro creado con éxito')->autoclose(3500);
        return redirect()->route('Categories.index');
    }


    //Display the specified resource.
    public function show($id)
    {
        $categories=Categories::find($id);
        return view('categories.show',compact('categories'));
    }


    //Show the form for editing the specified resource.
    public function edit($id)
    {
        //
        $categories=Categories::find($id);
        return view('categories.edit', compact('categories'));
    }


    //Update the specified resource in storage.
      public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'name'=> ['required',
            Rule::unique('categories', 'name')->ignore($id),
        ],
            'description'=> ['required']]);


        Categories::find($id)->update($request->all());
        alert()->success('Success Message', 'Registro modificado con éxito')->autoclose(3500);
        return redirect()->route('Categories.index');
    }

   
    //Remove the specified resource from storage.
    public function destroy($id)
    {
        //
        Categories::find($id)->delete();
        alert()->success('Success Message', 'Registro Eliminado Correctamente')->autoclose(3500);
        return redirect()->route('Categories.index');
    }
}
