<?php

namespace App\Models\Products;
use App\Image;
use App\Models\Categories\Categories;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    // se envian por medio de un array los datos que se quieren llenar
    protected $fillable=['category_id', 'description', 'price', 'user_id'];

    
    public function categoryname() // relacion 1 a 1 entre el modelo Product y el modelo Categories
    { 
        // Categories es el modelo donde esta la relacion, 'id' es el campo en Categories y 'category_id' es la relacion con products
        return $this->hasOne(Categories::class,'id','category_id');
    }


    public function image() // relacion 1 a 1 entre el modelo Product y el modelo Categories
    { 
        // Categories es el modelo donde esta la relacion, 'id' es el campo en Categories y 'category_id' es la relacion con products
        return $this->hasMany(Image::class,'product_id','id');
    }

}


