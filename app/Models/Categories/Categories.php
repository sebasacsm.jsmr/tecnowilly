<?php

namespace App\Models\Categories;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    // se envian por medio de un array los datos que se quieren llenar
    protected $fillable=['name', 'description', 'user_id'];
}