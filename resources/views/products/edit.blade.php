@extends('layouts.app')
@section('content')

<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-6">
      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-header">Actualizar Producto</div>
        <div class="card-body">
          <form method="POST" action="{{ route('products.update',$products->id) }}" role="form" enctype="multipart/form-data">
           {{ csrf_field() }}
           <input name="_method" type="hidden" value="PATCH"> 


           <div class="form-group row">
              <label class="col-md-3 col-form-label text-md-right">Categoría<span class="required" style="color:red">*</span></label>
              <div class="col-md-8">
              <input type="text" class="form-control" readonly value="{{$products->categoryname->name}}">
              <input name="category_id" type="hidden" class="form-control" readonly value="{{$products->categoryname->id}}">

              </div>
          </div>

          <div class="form-group row">
              <label for="name" class="col-md-3 col-form-label text-md-right">Descripción<span class="required" style="color:red">*</span></label>

              <div class="col-md-8">
                  <textarea name="description" class="form-control @error('description') is-invalid @enderror" placeholder="Ingrese una Descripción">{{$products->description}}</textarea>

                  @error('description')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror
              </div>
          </div>

          <div class="form-group row">
              <label class="col-md-3 col-form-label text-md-right">Precio<span class="required" style="color:red">*</span></label>
              <div class="col-md-8">
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">$</span>
                  </div>
                  <input id="price" type="text" min="0" class="form-control solo_numeros @error('#price') is-invalid @enderror" value="{{ number_format($products->price,0,',' ,'.')}}" required autocomplete="off">
                  <input name="price" id="price2" type="hidden" value="{{ $products->price}}">
                </div>
                  @error('name')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror
              </div>
          </div>

          <div class="form-group row">
              <label class="col-md-3 col-form-label text-md-right">Imágenes<span class="required" style="color:red">*</span></label>
              <div class="col-md-8">
                  <input class="form-control" type="file" name="img1">
                  <input class="form-control" type="file" name="img2">
                  <input class="form-control" type="file" name="img3">
              </div>
          </div>

          <div class="form-group row">
              <label class="col-md-3 col-form-label text-md-right"></label>
              <div class="col-md-8">
                  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">

                  <ol class="carousel-indicators">
                  @foreach( $products->image as $item )
                      <li data-target="#carouselExampleIndicators" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}"></li>
                  @endforeach
                  </ol>

                  <div class="carousel-inner" role="listbox">
                      @foreach( $products->image as $item )
                         <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                             <img class="d-block img-fluid style-carousel" src="{{ asset('storage/'.$item->path)}}" alt="{{ asset('storage/'.$item->path)}}">
                                <div class="carousel-caption d-none d-md-block">
                                </div>
                         </div>
                      @endforeach
                  </div>

                  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                  </a>
                  </div>
              </div>
          </div>

            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary">{{ __('Registrar') }}</button>
                    <a href="{{ route('products.index') }}" class="btn btn-danger" >Cancelar</a>
                </div>
            </div> 
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  /* Funcion para solo numeros en el input de precio */
  $(".solo_numeros").keyup(function (){
          this.value = formatNumber(this.value.replace(/[^0-9]/g,''));
          price = parseInt($('#price').val().replace(/\./g,''));
          $('#price2').val(price);
      });
  
    // funcion para miles con . y decimales con ,
    function formatNumber(n){
      n = Number(n)
      return new Intl.NumberFormat().format(n);
    }
  /* Fin Funcion Solo Numero */
  </script>
  
 @endsection
