@extends('layouts.app')
@section('content')

<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-6">
      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-header">Ver Producto</div>
        <div class="card-body">
        <form method="POST" role="form" enctype="multipart/form-data">
           {{ csrf_field() }}

            <div class="form-group row">
                <label class="col-md-3 col-form-label text-md-right">Categoría</label>
                <div class="col-md-8">
                <input type="text" class="form-control" readonly value="{{$products->categoryname->name}}">
                </div>
            </div>

             <div class="form-group row">
                <label for="name" class="col-md-3 col-form-label text-md-right">Descripción</label>

                <div class="col-md-8">
                    <textarea name="description" class="form-control" readonly>{{$products->description}}</textarea>
                </div>
            </div>
           
            <div class="form-group row">
                <label class="col-md-3 col-form-label text-md-right">Precio</label>
                <div class="col-md-8">
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text">$</span>
                    </div>
                    <input id="price" type="text" class="form-control solo_numeros" value="{{ number_format($products->price,0,',' ,'.')}}" readonly>
                    <input name="price" id="price2" type="hidden" value="">
                  </div>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 col-form-label text-md-right">Imágenes</label>
                <div class="col-md-8">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">

                    <ol class="carousel-indicators">
                    @foreach( $products->image as $item )
                        <li data-target="#carouselExampleIndicators" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}"></li>
                    @endforeach
                    </ol>

                    <div class="carousel-inner" role="listbox">
                        @foreach( $products->image as $item )
                           <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                               <img class="d-block img-fluid style-carousel" src="{{ asset('storage/'.$item->path)}}" alt="{{ asset('storage/'.$item->path)}}">
                                  <div class="carousel-caption d-none d-md-block">
                                  </div>
                           </div>
                        @endforeach
                    </div>

                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                    </div>
                </div>
            </div>

            <div class="form-group row mb-0" style="margin-left:55px;">
                <div class="col-md-6 offset-md-4">
                    {{-- <button type="submit" class="btn btn-primary">{{ __('Registrar') }}</button> --}}
                    <a href="{{ route('products.index') }}" class="btn btn-danger">Cancelar</a>
                </div>
            </div> 
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
 @endsection