@extends('layouts.app')

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">
  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Lista de Productos</h6>
    </div>
    <div class="card-body">
      <div class="table-responsive">
      <div class="btn-group">
      <a href="{{ route('Categories.create') }}" class="btn btn-info" data-toggle="modal" data-target="#ModalCreate"><i class="fa fa fa-plus-square"></i>&nbsp;&nbsp;Añadir Producto</a>
      </div>
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead class="thead-light">
            <tr>
              <th>Categoría</th>
              <th>Descripción</th>
              <th>Precio</th>
              <th>Ver</th>
              <th>Editar</th>
              <th>Eliminar</th>
            </tr>
          </thead>
         <!-- <tfoot>
            <tr>
              <th>Nombre</th>
              <th>Descripción</th>
              <th>Editar</th>
              <th>Eliminar</th>
            </tr>
          </tfoot> -->
          <tbody>
            @if($products->count())  
            @foreach($products as $product)  
            <tr>
              <td>{{$product->categoryname->name}}</td>
              <td>{{$product->description}}</td>
              <td>{{ '$'.number_format($product->price,0,',' ,'.')}}</td>

              <td style="width: 50px">
                <a class="btn btn-primary btn-2x" href="{{action('Products\ProductController@show', $product->id)}}">
                <i class="fa fa-eye"></i></a>
              </td>

              <td style="width: 50px">
                <a class="btn btn-primary btn-2x" href="{{action('Products\ProductController@edit', $product->id)}}">
                <i class="fa fa-pencil-square-o"></i></a>
              </td>

              <td style="width: 50px">
                <form action="{{action('Products\ProductController@destroy', $product->id)}}" method="post">
                 {{csrf_field()}}
                <input name="_method" type="hidden" value="DELETE">
                <button class="btn btn-danger btn-2x" type="submit"><i class="fa fa-bitbucket"></i></button>
                </form>
              </td>
            </tr>
             @endforeach 
             @else
             <tr>
              <td colspan="8">No hay Productos creados!</td>
            </tr>
            @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<!--Modal Create Categories -->

  <!-- Modal -->
  <div class="modal fade" id="ModalCreate" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Crear Producto</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
          <form method="POST" action="{{ route('products.store') }}" role="form" enctype="multipart/form-data">
           {{ csrf_field() }}

            <div class="form-group row">
                <label class="col-md-3 col-form-label text-md-right">Categoría<span class="required" style="color:red">*</span></label>
                <div class="col-md-8">

                <select name="category_id" class="form-control @error('category') is-invalid @enderror" required>
                  <option value="">-Seleccione una opción-</option>
                    @foreach($categories as $category)
                    <option value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                </select>
                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

             <div class="form-group row">
                <label for="name" class="col-md-3 col-form-label text-md-right">Descripción<span class="required" style="color:red">*</span></label>

                <div class="col-md-8">
                    <textarea name="description" class="form-control @error('description') is-invalid @enderror" placeholder="Ingrese una Descripción"></textarea>

                    @error('description')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
           
            <div class="form-group row">
                <label class="col-md-3 col-form-label text-md-right">Precio<span class="required" style="color:red">*</span></label>
                <div class="col-md-8">
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text">$</span>
                    </div>
                    <input id="price" type="text" min="0" class="form-control solo_numeros @error('price') is-invalid @enderror" value="{{ old('price') }}" required autocomplete="off">
                    <input name="price" id="price2" type="hidden" value="">
                  </div>
                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 col-form-label text-md-right">Imágenes<span class="required" style="color:red">*</span></label>
                <div class="col-md-8">
                    <input class="form-control" type="file" name="img1">
                    <input class="form-control" type="file" name="img2">
                    <input class="form-control" type="file" name="img3">
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary">{{ __('Registrar') }}</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">{{ __('Cerrar') }}</button>
                </div>
            </div> 
          </form>
          </div>
        </div>
     </div>
  </div>
<!--End Modal Create Categories -->

<script type="text/javascript">
  $(document).ready(function(){
     $('#dataTable').DataTable({
      "pageLength":5,
      "aLengthMenu": [[5,10,25, 50],[5,10,25, 50, "All"]],

        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },

     }); 
  })

/* Funcion para solo numeros en el input de precio */
$(".solo_numeros").keyup(function (){
        this.value = formatNumber(this.value.replace(/[^0-9]/g,''));
        price = parseInt($('#price').val().replace(/\./g,''));
        $('#price2').val(price);
    });

  // funcion para miles con . y decimales con ,
  function formatNumber(n){
    n = Number(n)
    return new Intl.NumberFormat().format(n);
  } 

/* Fin Funcion Solo Numero */
</script>



@endsection
