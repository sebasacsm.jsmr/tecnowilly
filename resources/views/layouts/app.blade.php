<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>TecnoWilly</title>
    <link rel="icon" type="image/png" href="{{ asset('/img/pc.png') }}" />


    <!-- Scripts -->
    <!-- <script src="{{ asset('js/app.js') }}" defer></script>  -->


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

     <!-- Icon -->
    <link rel="stylesheet" href="{{ asset('/css/font-awesome.min.css') }}">


    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="{{ asset('/css/sb-admin-2.min.css') }}">

    <!-- jquery -->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>

    <!--sweetalert -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <!-- Image_responsive -->
    <link rel="stylesheet" href="{{ asset('/css/image_responsive.css') }}">

</head>

<body id='page-top'>
    <div id="wrapper">
      @include('layouts.menu')

      <!-- Content Wrapper -->
      <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">
          <!-- Navbar Laravel-->
          @include('layouts.navbar')

          <!-- Begin Page Content -->
          <div class="container-fluid">

            <!-- Content Row -->
            <div class="row">
              @yield('content')
            </div>
          </div>
        </div>

        <!-- Footer -->
        <footer class="sticky-footer bg-white">
          <div class="container my-auto">
            <div class="copyright text-center my-auto">
              <span>Copyright &copy; Proyecto de Grado - Uniminuto 2019</span>
            </div>
          </div>
        </footer>
        <!-- End of Footer -->

      </div>
    </div>

   
   <!-- Bootstrap core JavaScript-->
  {{-- <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script> --}}
  <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>
    
  <!-- Core plugin JavaScript-->
  <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}" type="text/javascript"></script>

  <!-- Custom scripts for all pages-->
  <script  href="{{ asset('js/sb-admin-2.min.js') }}" type="text/javascript"></script>

  <!-- Page level plugins -->
  <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}" type="text/javascript"></script>
  
  <!-- Include this after the sweet alert js file -->
    @include('sweet::alert')
</body>
</html>
