@extends('layouts.app')
@section('content')

<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-6">
      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-header">Actualizar Usuario</div>
        <div class="card-body">
          <form method="POST" action="{{ route('user.update',$users->id) }}">
             {{ csrf_field() }}
          <input name="_method" type="hidden" value="PATCH"> <!-- define que la ruta establecida en el from va con PUT o PATCH-->
              <div class="form-group row">
                  <label for="name" class="col-md-4 col-form-label text-md-right">Nombre<span class="required" style="color:red">*</span></label>
                  <div class="col-md-6">
                      <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{$users->name}}" required autocomplete="name" autofocus>
                      @error('name')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
              </div>

              <div class="form-group row">
                  <label for="email" class="col-md-4 col-form-label text-md-right">Correo Electrónico<span class="required" style="color:red">*</span></label>
                  <div class="col-md-6">
                      <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{$users->email}}" required autocomplete="email">
                      @error('email')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
              </div>

              <div class="form-group row">
                  <label for="password" class="col-md-4 col-form-label text-md-right">Contraseña<span class="required" style="color:red">*</span></label>
                  <div class="col-md-6">
                      <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                      @error('password')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
              </div>

              <div class="form-group row">
                  <label for="password-confirm" class="col-md-4 col-form-label text-md-right">Confirmar Contraseña<span class="required" style="color:red">*</span></label>

                  <div class="col-md-6">
                      <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                  </div>
              </div>

            <div class="form-group row mb-0">
              <div class="col-md-6 offset-md-4">
                <input type="submit"  value="Actualizar" class="btn btn-primary">
                <a href="{{ route('user.index') }}" class="btn btn-danger" >Cancelar</a>
              </div> 
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>
 @endsection