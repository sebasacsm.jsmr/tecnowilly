@extends('layouts.app')

@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">
  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Lista de Usuarios</h6>
    </div>
    <div class="card-body">
        <div class="btn-group">
            <a class="btn btn-info" style="color:white"; data-toggle="modal" data-target="#ModalCreate"><i class="fa fa fa-plus-square"></i>&nbsp;&nbsp;Crear Usuario</a>
        </div>
      <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead class="thead-light">
                <tr>
                  <th>Nombre</th>
                  <th>Correo</th>
                  <th>Editar</th>
                  <th>Eliminar</th>
                </tr>
            </thead>
          <tbody>
            @if($users->count())  
            @foreach($users as $u)  
            <tr>
              <td>{{$u->name}}</td>
              <td>{{$u->email}}</td>
              <td style="width: 50px">
                <a class="btn btn-primary btn-2x" href="{{action('Users\UserController@edit', $u->id)}}">
                <i class="fa fa-pencil-square-o"></i>
                </a>
              </td>
                  
              <td style="width: 50px">
                <form action="{{action('Users\UserController@destroy', $u->id)}}" method="post">
                 {{csrf_field()}}
                <input name="_method" type="hidden" value="DELETE">
                <button class="btn btn-danger btn-2x" type="submit"><i class="fa fa-bitbucket"></i></button>
                </form>
              </td>
            </tr>
             @endforeach 
             @else
             <tr>
              <td colspan="8">No hay Usuarios creados!</td>
            </tr>
            @endif
          </tbody>

        </table>
      </div>
    </div>
  </div>
</div>


<!--Modal Create Usuarios -->

  <!-- Modal -->
<div class="modal fade" id="ModalCreate" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Crear Usuario</h4>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ route('register') }}">
                    @csrf

                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">Nombre<span class="required" style="color:red">*</span></label>
                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="email" class="col-md-4 col-form-label text-md-right">Correo Electrónico<span class="required" style="color:red">*</span></label>
                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password" class="col-md-4 col-form-label text-md-right">Contraseña<span class="required" style="color:red">*</span></label>
                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password-confirm" class="col-md-4 col-form-label text-md-right">Confirmar Contraseña<span class="required" style="color:red">*</span></label>
                        <div class="col-md-6">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">{{ __('Registrar') }}</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">{{ __('Cerrar') }}</button>
                        </div>
                    </div> 
                </form>
            </div>
        </div>
    </div>
</div>
<!--End Modal Create Categories -->


<script type="text/javascript">
  $(document).ready(function(){
     $('#dataTable').DataTable({
      "pageLength":5,
      "aLengthMenu": [[5,10,25, 50],[5,10,25, 50, "All"]],

        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },

     }); 
  })
</script>

<!-- /.container-fluid -->
@endsection

