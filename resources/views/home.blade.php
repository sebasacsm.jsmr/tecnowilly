@extends('layouts.app')

@section('content')
 <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Content Row -->
          <div class="row">
            <div class="col-lg-12 mb-4">

              <!-- Presentación -->
              <div class="card shadow mb-2">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">QUIÉNES SOMOS</h6>
                </div>
                <div class="card-body">
                  <div class="text-center">
                    <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 25rem;" src="img/TW.png" alt="">
                  </div>
                  <p>TecnoWilly fue creado en el año 2015, para ofrecer servicios de soporte técnico especializado, con experiencia, responsabilidad y profesionalismo en el mantenimiento preventivo y correctivo de computadores y demás dispositivos informáticos</p>
                </div>
              </div>

              <!-- Grupo Card -->
              <div class="row">
                <div class="card-group">

                  <!-- Misión -->
                  <div class="col-sm-6">
                    <div class="card shadow mb-4">
                      <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">MISIÓN</h6>
                      </div>
                      <div class="card-body">
                        <p>Nuestra misión es ofrecer un excelente servicio de soporte técnico especializado, con experiencia, responsabilidad y profesionalismo en el menor tiempo posible.</p>
                      </div>
                    </div>
                  </div>

                  <!-- Visión -->
                  <div class="col-sm-6">
                    <div class="card shadow mb-4">
                      <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">MISIÓN</h6>
                      </div>
                      <div class="card-body">
                        <p>Para el año 2023 Tecnowilly, espera aumentar el flujo de clientes e incrementar las utilidades en un 50% anual, provenientes del sector de las empresas y hogares en Colombia.</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
@endsection



