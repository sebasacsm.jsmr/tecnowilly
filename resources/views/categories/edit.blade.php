@extends('layouts.app')
@section('content')

<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-6">
      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-header">Actualizar Categoría</div>
        <div class="card-body">
          <form method="POST" action="{{ route('Categories.update',$categories->id) }}"  role="form">
           {{ csrf_field() }}
           <input name="_method" type="hidden" value="PATCH"> 

            <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">Nombre<span class="required" style="color:red">*</span></label>

                <div class="col-md-6">
                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{$categories->name}}" required autocomplete="name" autofocus>

                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

             <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">Descripcion<span class="required" style="color:red">*</span></label>

                <div class="col-md-6">
                    <textarea name="description" class="form-control @error('description') is-invalid @enderror" placeholder="Ingrese una Descripción">{{$categories->description}}</textarea>

                    @error('description')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary">{{ __('Registrar') }}</button>
                    <a href="{{ route('Categories.index') }}" class="btn btn-danger" >Cancelar</a>
                </div>
            </div> 
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
 @endsection