@extends('layouts.app')

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">
  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Lista de Categorías</h6>
    </div>
    <div class="card-body">
      <div class="table-responsive">
      <div class="btn-group">
      <a href="{{ route('Categories.create') }}" class="btn btn-info" data-toggle="modal" data-target="#ModalCreate"><i class="fa fa fa-plus-square"></i>&nbsp;&nbsp;Añadir Categoría</a>
      </div>
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead class="thead-light">
            <tr>
              <th>Nombre</th>
              <th>Descripción</th>
              <th>Editar</th>
              <th>Eliminar</th>
            </tr>
          </thead>
         <!-- <tfoot>
            <tr>
              <th>Nombre</th>
              <th>Descripción</th>
              <th>Editar</th>
              <th>Eliminar</th>
            </tr>
          </tfoot> -->
          <tbody>
            @if($categories->count())  
            @foreach($categories as $category)  
            <tr>
              <td>{{$category->name}}</td>
              <td>{{$category->description}}</td>

              <td style="width: 50px">
                <a class="btn btn-primary btn-2x" href="{{action('Categories\CategoriesController@edit', $category->id)}}">
                <i class="fa fa-pencil-square-o"></i></a>


              <td style="width: 50px">
                <form action="{{action('Categories\CategoriesController@destroy', $category->id)}}" method="post">
                 {{csrf_field()}}
                <input name="_method" type="hidden" value="DELETE">
                <button class="btn btn-danger btn-2x" type="submit"><i class="fa fa-bitbucket"></i></button>
                </form>
              </td>
            </tr>
             @endforeach 
             @else
             <tr>
              <td colspan="8">No hay Categorías creadas!</td>
            </tr>
            @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<!--Modal Create Categories -->

  <!-- Modal -->
  <div class="modal fade" id="ModalCreate" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Crear Categoría</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
          <form method="POST" action="{{ route('Categories.store') }}"  role="form">
           {{ csrf_field() }}

            <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">Nombre<span class="required" style="color:red">*</span></label>

                <div class="col-md-6">
                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

             <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">Descripcion<span class="required" style="color:red">*</span></label>

                <div class="col-md-6">
                    <textarea name="description" class="form-control @error('description') is-invalid @enderror" placeholder="Ingrese una Descripción"></textarea>

                    @error('description')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary">{{ __('Registrar') }}</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">{{ __('Cerrar') }}</button>
                </div>
            </div> 
          </form>
          </div>
        </div>
     </div>
  </div>
<!--End Modal Create Categories -->


<script type="text/javascript">
  $(document).ready(function(){
     $('#dataTable').DataTable({
      "pageLength":5,
      "aLengthMenu": [[5,10,25, 50],[5,10,25, 50, "All"]],

        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },

     }); 
  })
</script>

<!-- /.container-fluid -->
@endsection