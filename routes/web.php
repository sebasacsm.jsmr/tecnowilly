<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route Categories
Route::resource('Categories', 'Categories\CategoriesController')->middleware('auth');
Route::resource('user', 'Users\UserController')->middleware('auth');
Route::resource('products', 'Products\ProductController')->middleware('auth');

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
